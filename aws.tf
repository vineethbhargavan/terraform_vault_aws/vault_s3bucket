provider "aws" {
  region = var.aws_region
}

resource "aws_iam_user" "secrets_engine" {
  name = "${var.environment}-hcp-vault-secrets-engine"
}

resource "aws_iam_access_key" "secrets_engine_credentials" {
  user = aws_iam_user.secrets_engine.name
}

resource "aws_iam_user_policy" "vault_secrets_engine_generate_credentials" {
  name = "${var.environment}-hcp-vault-secrets-engine-policy"
  user = aws_iam_user.secrets_engine.name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "sts:AssumeRole",
        ]
        Effect   = "Allow"
        Resource = "${aws_iam_role.tfc_role.arn}"
      },
    ]
  })
}

resource "aws_iam_role" "tfc_role" {
  name = "${var.environment}-tfc-role"

  assume_role_policy = jsonencode(
    {
      Statement = [
        {
          Action    = "sts:AssumeRole"
          Condition = {}
          Effect    = "Allow"
          Principal = {
            AWS = aws_iam_user.secrets_engine.arn
          }
        },
      ]
      Version = "2012-10-17"
    }
  )
}

resource "aws_iam_policy" "tfc_policy" {
  name        = "${var.environment}-tfc-policy"
  description = "TFC run policy"

  policy = jsonencode({
    Statement = [
      {
        Action = [
          "lambda:*",
          "ec2:*",
          "iam:*",
          "cloudwatch:*",
          "apigateway:*",
          "s3:*",
          "logs:*"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "tfc_policy_attachment" {
  role       = aws_iam_role.tfc_role.name
  policy_arn = aws_iam_policy.tfc_policy.arn
}



# create s3 bucket for Terraform cloud State management - with versioning.
resource "aws_s3_bucket" "terraform_bucket" {
  bucket = "${var.environment}-terraformstatefile"  # Replace with your desired bucket name
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"  
      }
    }
  }
}

# acl is public to facilitate access by terraform cloud. To Secure it - ideally we can make it private and whitelist it using the IP of terraform agent!!!
