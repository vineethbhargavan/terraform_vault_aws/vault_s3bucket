## Setting up Vault and S3 Bucket(to Store terraform cloud state)
### Vault terraform reference:
# this repo create - a secret vault - of the type - aws.
# ref : https://developer.hashicorp.com/terraform/tutorials/cloud/vault-backed-dynamic-credentials


## Pre-requisities
- Ensure you have a vault cluster in place. Refer <>
- Create a Project in Terraform Cloud to organise the workspaces. This will be required during terraform run.
- Ensure you have Terraform cloud and the vcs integration already in place. (oauth id will be required).
- vcs_repo : Ensure you have a vcs repo created to host the terraform code which will create the required infra in aws

## Environment variables to set
- TFE_TOKEN=XX
- VAULT_TOKEN=XX (only valid for 6 hours)
- AWS_ACCESS_KEY_ID=xx
- AWS_SECRET_ACCESS_KEY=xx


## Other Variables to feed in during Terraform Run.
- environment = dev
- tfc_project_name = sbs
- tfc_workspace_name = spa_lambda
- vault_url = https://vineeth-vault-cluster-public-vault-4855bb09.062a2ed3.z1.hashicorp.cloud:8200/
- vault_namespace = admin/dev
- vcs_repo = 

## Snapshot from Terraform variables page.
environment
dev	terraform	

tfc_project_name
sbs	terraform	

tfc_workspace_name
aws_lambda_spa	terraform	

vault_namespace
admin/dev	terraform	

vault_url
https://vineeth-vault-cluster-public-vault-4855bb09.062a2ed3.z1.hashicorp.cloud:8200/	terraform	

vcs_repo
terraform_vault_aws/aws_lambda_spa	terraform	

AWS_ACCESS_KEY_ID
AKIAW4MZ4HZ5DUME76TO	env	

AWS_SECRET_ACCESS_KEY
SENSITIVE	Sensitive - write only	env	

TFE_TOKEN
SENSITIVE	Sensitive - write only	env	

VAULT_TOKEN
valid only for 6 hours

hvs.CAESID_0MYKjOfmO7ezlKf2jB5q5bVGfhWKIDY3U9LaNeEcfGigKImh2cy5KMVI2WFFKTkk2S1FFcXcxZkkyallUdHkuVXRhbWkQx7YX	env


## Challenges
- Creating Namespace and using its reference for other resources gave 403 errors.
- For instance if a namespace : admin/dev is created. The output of the namespace resource gives : admin/dev/ . There is a trailing slash! Potentially this created the conflict. Obviously there are ways of trimming the trailing slash using a local variable...but the interesting thing is ...i do have working code in my other personal projects where there were no issues in using the namespace reference!! ..maybe its the terraform version!! ...something to ponder for a later time!
- Error: error writing to Vault: Error making API request. Namespace: devsbs URL: PUT https://vineeth-vault-cluster-public-vault-4855bb09.062a2ed3.z1.hashicorp.cloud:8200/v1/sys/policies/acl/tfc-policy Code: 403. Errors: * 1 error occurred: * permission denied
with vault_policy.tfc_policy
on vault.tf line 57, in resource "vault_policy" "tfc_policy":
resource "vault_policy" "tfc_policy" {