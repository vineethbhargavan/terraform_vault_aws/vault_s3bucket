variable "environment" {
  type        = string
  description = "environment"
}


# repo settings
variable "vcs_repo" {
  type        = string
  description = "vcs repo"
}

variable "vcs_org" {
  type        = string
  default     = "VineethBhargavan"
  description = "vcs org"
}


variable "aws_secret_backend_role_name" {
  type        = string
  default =  "vault-access-role"
  description = "Name of AWS secret backend role for runs to use"
}

variable "tfc_hostname" {
  type        = string
  default     = "app.terraform.io"
  description = "The hostname of the TFC or TFE instance you'd like to use with Vault"
}

variable "tfc_organization_name" {
  type        = string
  default = "vineethworkspace"
  description = "The name of your Terraform Cloud organization"
}

variable "tfc_project_name" {
  type        = string
  description = "The project under which a workspace will be created"
}

variable "tfe_project_id" {
  type        = string
  description = "The project ID under which a workspace will be created"
}

variable "tfc_workspace_name" {
  type        = string
  description = "The name of the workspace that you'd like to create and connect to AWS"
}

# Variables for Vault

variable "vault_url" {
  type        = string
  description = "The URL of the Vault instance you'd like to use with Terraform Cloud"
}

variable "jwt_backend_path" {
  type        = string
  default     = "jwt"
  description = "The path at which you'd like to mount the jwt auth backend in Vault"
}

variable "vault_namespace" {
  type        = string
  description = "The namespace of the Vault instance you'd like to create the AWS and jwt auth backends in"
}

variable "tfc_vault_audience" {
  type        = string
  default     = "vault.workload.identity"
  description = "The audience value to use in run identity tokens"
}

variable "aws_region" {
  type        = string
  default     = "ap-southeast-2"
  description = "AWS region for all resources"
}
